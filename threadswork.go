package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {

	placesLen, clientslen := GetSettings()
	clients := GenerateClients(clientslen)
	places := GeneratePlaces(placesLen)
	mute := sync.Mutex{}

	gameData := FullData{
		places:  places,
		clients: clients,
		worker: Worker{
			name:           "bob",
			IsSleeping:     true,
			EstimatedTicks: -1,
		},
		workingTable: WorkingTable{
			IsEmpty:  true,
			PlacedBy: -1,
		},
	}

	for !(CheckOnClients(gameData.clients)) {
		ch := make(chan FullData, 1)
		ch <- gameData

		wg.Add(1)
		go func() {

			defer wg.Done()
			time.Sleep(100 * time.Millisecond)

			mute.Lock()
			gameData = Work(gameData)
			mute.Unlock()
		}()
		clients := GetUnsatisfiedAndFree(gameData)

		if len(clients) > 0 {
			checking := clients[rand.Intn(len(clients)-0)+0]
			gameData = NextTurn(gameData, checking)
		}

		// worker is trying to sleep on place
		gameData = TrySleep(gameData)
		// if places not empty clients will try to wake up him
		gameData = CheckOnSleeping(gameData)

		// worker finds new client
		gameData = FindNextClient(gameData)
		wg.Wait()
	}

}

func FindNextClient(gm FullData) FullData {
	if gm.workingTable.IsEmpty {

		plsCh := NotEmptyPlaces(gm)
		choosen := Place{}
		if len(plsCh) >= 1 {
			for _, place := range plsCh {
				if !gm.clients[place.placedBy].IsSatisfied {
					choosen = place
				}
			}

			// setting client on working table
			gm.workingTable.IsEmpty = false
			gm.workingTable.PlacedBy = choosen.placedBy
			// freeing place from him
			for i, place := range gm.places {
				if place.placedBy == choosen.placedBy {
					gm.places[i].IsEmpty = true
					gm.places[i].placedBy = -1
				}
			}

			gm.worker.EstimatedTicks = 4
			fmt.Printf("\nworker %s getting ready next client %d", gm.worker.name, choosen.placedBy)
			return gm
		}
	}
	return gm
}

func CheckOnSleeping(gm FullData) FullData {
	plsCh := NotEmptyPlaces(gm)
	if len(plsCh) >= 1 {
		choosen := plsCh[rand.Intn(len(plsCh)-0)+0]
		if gm.worker.IsSleeping {
			gm.worker.IsSleeping = false
			for _, place := range gm.places {
				if place.placedBy == -2 {
					place.IsEmpty = true
					place.placedBy = -1
					fmt.Printf("\nclient %s find out thats worker %s is sleeping so he punched him and force back to work", choosen.placedBy, gm.worker.name)
					return gm
				}
			}
		}
	}
	return gm
}

func TrySleep(gm FullData) FullData {
	if (gm.workingTable.IsEmpty) && !(gm.worker.IsSleeping) {
		if CheckOnPlaces(gm.places) {
			for i, place := range gm.places {
				if place.IsEmpty {
					gm.places[i].IsEmpty = false
					gm.worker.IsSleeping = true
					gm.places[i].placedBy = -2
					fmt.Printf("\nworker %s is sleeping on place %d while no one sees it", gm.worker.name, place.uuid)
					return gm
				}
			}
		} else {
			fmt.Printf("\nworker %s cant sleep cause there no free places for him", gm.worker.name)
		}
	}
	return gm
}

func Work(gm FullData) FullData {

	if gm.worker.EstimatedTicks >= 1 {
		fmt.Printf("\nworker %s now working on client %d he needs %d ticks to do", gm.worker.name, gm.workingTable.PlacedBy, gm.worker.EstimatedTicks)
		gm.worker.EstimatedTicks--
	} else if gm.worker.EstimatedTicks == 0 {
		fmt.Printf("\nworker %s ended work on client %d. now he will try to sleep", gm.worker.name, gm.workingTable.PlacedBy)
		gm.clients[gm.workingTable.PlacedBy].IsSatisfied = true
		gm.workingTable.PlacedBy = -1
		gm.workingTable.IsEmpty = true
	}
	return gm
}

func NextTurn(gm FullData, client Client) FullData {
	if !client.IsSatisfied {
		fmt.Printf("\nclient %d is looking for free spot", client.uuid)
		if CheckOnPlaces(gm.places) {
			for i, place := range gm.places {
				if place.IsEmpty {
					gm.places[i].placedBy = client.uuid
					gm.places[i].IsEmpty = false
					fmt.Printf("\nclient %d found free spot %d", client.uuid, place.uuid)
					//close(gd)
					return gm
				}
			}
		} else {
			fmt.Printf("\nclient %d cant find free space", client.uuid)
		}
	}
	//time.Sleep(3 * time.Second)
	return gm
}

func NotEmptyPlaces(gm FullData) []Place {
	places := []Place{}

	for _, place := range gm.places {
		if !(place.IsEmpty) && (place.placedBy != -2) {
			places = append(places, place)
		}
	}
	return places
}

// looking for unsatisfied clients that has not entered in place yet
func GetUnsatisfiedAndFree(gm FullData) []Client {
	clients := []Client{}
	for _, client := range gm.clients {
		// was he already here
		if !client.IsSatisfied {
			OnPlace := false
			// is he on place already
			for _, place := range gm.places {
				if place.placedBy == client.uuid {
					OnPlace = true
				}
			}
			if !OnPlace {
				clients = append(clients, client)
			}
		}
	}
	return clients
}

// generating places
func GeneratePlaces(placesLen int) []Place {
	places := []Place{}
	for i := 0; i < placesLen; i++ {
		place := Place{
			uuid:     i,
			placedBy: -1,
			IsEmpty:  true,
		}
		places = append(places, place)
	}
	return places
}

// generating clients
func GenerateClients(clientsLen int) []Client {
	clients := []Client{}
	for i := 0; i < clientsLen; i++ {
		client := Client{
			uuid:        i,
			IsSatisfied: false,
		}
		clients = append(clients, client)
	}
	return clients
}

// looking for free places
func CheckOnPlaces(places []Place) bool {
	for i := 0; i < len(places); i++ {
		if places[i].IsEmpty {
			return true
		}
	}
	return false
}

// looking for unsatisfied clients
func CheckOnClients(clients []Client) bool {
	for i := 0; i < len(clients); i++ {
		if !(clients[i].IsSatisfied) {
			return false
		}
	}
	return true
}
