package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

func ReadCsv() [][]string {
	file, err := os.Open("settings.csv")
	if err != nil {
		fmt.Println(err)
	}
	reader := csv.NewReader(file)
	records, _ := reader.ReadAll()
	return records
}

func GetSettings() (int, int) {
	records := ReadCsv()
	return strToiInt(records[1][0]), strToiInt(records[1][1])
}
