package main

import "sync"

type FullData struct {
	worker       Worker
	clients      []Client
	places       []Place
	workingTable WorkingTable
	// i'm not expecting this can get deadlock but i leave it here still
	Mutex sync.Mutex
}

type Place struct {
	uuid    int
	IsEmpty bool
	// id client
	placedBy int
}

type Worker struct {
	IsSleeping bool
	// worker name... can be deleted cause its useless
	name string
	// id client on which we are working on now
	EstimatedTicks int
}

type Client struct {
	uuid        int
	IsSatisfied bool
}

type WorkingTable struct {
	IsEmpty  bool
	PlacedBy int
}
